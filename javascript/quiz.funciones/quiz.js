var quiz = [
    ["Cúal es el nombre real de Superman?", "Clark Kent"],
    ["Cúal es el nombre real de Spiderman?", "Peter Parker"],
    ["Cúal es el nombre real de Batman?", "Bruce Wayne"],
    ["Cúal es el nombre real de Wonderwoman?", "Diana Prince"],
];

var score = 0;

play(quiz);

function play(quiz){
    for(var i = 0, pregunta, respuesta, max=quiz.length; i < max; i++ ){
        var pregunta = quiz[i][0];
        var respuesta = preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();

    function preguntar(pregunta){
        return prompt(pregunta);
    }
    function verificar(respuesta){
        if(respuesta === quiz[i][1]){
            alert("Respuesta Correcta");
            score += 10;
        }else{
            alert("Respuesta Incorrecta");
            score -= 5;
        }
    }
    function gameOver(){
        alert("Game Over, Su puntuación fue " + score + " puntos");
    }
}