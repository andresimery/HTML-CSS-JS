var quiz = {
    "nombre": "Quiz de Nombres de Super Heroes",
    "descripcion": "Cuantos nombres de heroes conoces?",
    "pregunta": "Cúal es el nombre real de ",
    "preguntas": [
        {"pregunta": "Superman?", "respuesta": "Clark Kent"},
        {"pregunta": "Spiderman?", "respuesta": "Peter Parker"},
        {"pregunta": "Batman?", "respuesta": "Bruce Wayne"},
        {"pregunta": "Wonderwoman?", "respuesta": "Diana Prince"}
    ]
}

var score = 0;

var $pregunta = document.getElementById("pregunta");
var $score = document.getElementById("score");
var $feedback = document.getElementById("feedback");
var $start = document.getElementById("start")

$start.addEventListener("click", function(){play(quiz)}, false);

function update(elemento, contenido, estilo){
    var p = elemento.firstChild || document.createElement("p");
    p.textContent = contenido;
    elemento.appendChild(p);
    if(estilo){
        p.className = estilo;
    }
}

function play(quiz){
    for(var i = 0, pregunta, respuesta, max=quiz.preguntas.length; i < max; i++ ){
        var pregunta = quiz.preguntas[i].pregunta;
        var respuesta = preguntar(pregunta);
        verificar(respuesta);
    }
    gameOver();

    function preguntar(pregunta){
        update($pregunta, quiz.pregunta + pregunta);
        return prompt("Ingrese Su Respuesta");
    }
    function verificar(respuesta){
        if(respuesta === quiz.preguntas[i].respuesta){
            //alert("Respuesta Correcta");
            update($feedback, "Respuesta Correcta", "bien");
            score += 10;
            update($score, score);
        }else{
            //alert("Respuesta Incorrecta");
            update($feedback, "Respuesta Incorrecta", "mal");
            score -= 5;
            update($score, score);
        }
    }
    function gameOver(){
        //alert("Game Over, Su puntuación fue " + score + " puntos");
        update($pregunta, "Game Over, Su puntuación fue " + score + " puntos");
    }
}