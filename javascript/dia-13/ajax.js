var http = new XMLHttpRequest();

var $text = document.getElementById("text");
var $html = document.getElementById("html");

/*
// GET, POST PUT, PATCH, DELETE
http.open('GET', 'procesar.php', true);
http.onreadystatechange = procesarP;
http.send("nombre=andres"); // Formato Clave=Valor
//http.send("nombre:andres"); // Formato JSON

function procesarP(){
    if(http.readyState === 4 && http.status === 200){
        console.log("Pagina Encontrada");
        document.write(http.response);
    }
};*/




$text.addEventListener("click", function(){
    peticionTxt('texto.txt');
});
$html.addEventListener("click", function(){
    peticionHTML('https://www.google.com');
});

function peticionTxt(url){
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 200){
            console.log("Encontrado");
            document.getElementById('salida').innerHTML = http.responseText;
        }
    }

    http.open('GET', url, true);
    http.send();
    document.getElementById('salida').innerHTML = 'cargando...';
}

function peticionHTML(url){
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 200){
            console.log("Encontrado");
            document.getElementById('salida').innerHTML = http.responseText;
        }
    }

    http.open('GET', url, true);
    http.send();
    document.getElementById('salida').innerHTML = 'cargando...';
}

var $json = document.getElementById('json');
$json.addEventListener("click", function(){
    console.log("Click")
    enviarHeroe("{name:'Clark Kent'}");
});

function enviarHeroe(heroe){
    var http = new XMLHttpRequest();
    http.open('POST', 'procesar.php', true);
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function(){
        if(http.readyState === 4 && http.status === 201){
            console.log("Encontrado");
            console.log(http.responseText);
        }
    }
    http.send(JSON.stringify(heroe));
}