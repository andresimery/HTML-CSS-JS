var parrafo = document.getElementsByTagName("P");

console.log(parrafo);

parrafo[0].innerHTML = "Hola desde javascript";

console.log(document.body);


forms = document.forms;

for(var i = 0; i < forms.length; i ++){
    console.log(forms[i].id);
    form = forms[i];
    form.append("<p>Hola " + i + "</p>");
}

form = document.getElementById("id1");
form.style.background = "red";

lista = document.getElementById("lista"); // Un elemento
lista2 = document.getElementsByName("lista2"); // Coleccion
listas = document.getElementsByClassName("lista"); // Coleccion
listas2 = document.getElementsByTagName("ul");

body = document.querySelector("body");
console.log(body);

lista = document.querySelector("#lista");
console.log(lista.innerHTML = "<li>UML</li>");

p = document.querySelector("p");
console.log(p.firstChild);

lista = document.querySelector("ul");
console.log(lista);
console.log(lista.getAttribute("id"))
console.log(lista.setAttribute("class", "textoNaranja"));


p1 = document.createElement("p");
txtP1 = document.createTextNode("Texto Parrafo 1");
p1.appendChild(txtP1);
document.body.appendChild(p1);

p2 = document.createElement("p");
txtP2 = document.createTextNode("Texto Parrafo 2");
p2.appendChild(txtP2);
document.body.appendChild(p2, p1);


title = document.querySelector("title");
console.log(title.innerHTML = "Hola Mundo");

ul = document.querySelector(".lista")
document.body.removeChild(ul);

p = document.getElementsByTagName("p");
textoViejo = p[0].firstChild;
console.log(textoViejo);
nuevoTexto = document.createTextNode("Superman");
console.log(nuevoTexto);
p[0].replaceChild(nuevoTexto, textoViejo);

console.log(p[0].innerHTML)