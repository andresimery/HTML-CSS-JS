var div = document.getElementById('caja');
/*div.addEventListener('click', function(event){
    // event es la informacion del evento
    div.style.background = "#000";
    div.style.position = "absolute";
    div.style.top = "10%";
    div.style.left = "10%";
    console.log(event.target.id); // target es la informacion del elemento
});


div.addEventListener('mouseover', function(){
    div.style.background = "#456";
    div.style.position = "absolute";
    div.style.top = "20%";
    div.style.left = "40%";
});*/

var medida = 1;
/*div.addEventListener('mousemove', function(e){
    div.style.position = "relative";
    medida += 1;
    div.style.left = medida + "px";
    console.log(div.style.left)
});

addEventListener('keydown', function detener(event){
    div.style.position = "relative";
    if(event.keyCode >= 48 && event.keyCode <= 57){
        console.log(event.key);
    }else{
        console.log("Tecla: " + event.key + " Codigo: " + event.keyCode);
    };
    if(event.keyCode == 37){
        medida -= 1;
        div.style.left = medida + "px";
    }
    if(event.keyCode == 38){
        medida -= 1;
        div.style.top = medida + "px";
    }
    if(event.keyCode == 39){
        medida += 1;
        div.style.left = medida + "px";
    }
    if(event.keyCode == 40){
        medida += 1;
        div.style.top = medida + "px";
    }
});*/

addEventListener("keydown", function(event){
    if(event.keyCode == 32 && event.ctrlKey){
        console.log("Accion Cancelada")
    }
    if(event.shiftKey){
        console.log("Tecla Shift");
    }
});

div.addEventListener("click", function(evt){
    console.log("Evento Click");
    div.removeEventListener("click", function(){
        console.log("Evento Removido")
    });
});

var broken = document.getElementById("broken");
broken.addEventListener('click', function(event){
    event.preventDefault();
    console.log("Enlace Roto");
});

var ul = document.getElementById("lenguajes");
var li = document.querySelector("#lenguajes li")
ul.addEventListener("click", function(event){
    console.log("Haciendo Click en un UL")
});

li.addEventListener("click", function(event){
    console.log("Haciendo Click en un LI")
});