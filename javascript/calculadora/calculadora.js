window.onload = function(){ // Acciones tras cargar la pagina
    pantalla = document.getElementById('textoPantalla');
    document.onkeydown = teclado;   
};

var x = "0"; // guardar numero en pantalla
var xi = 1; // iniciar numero en pantalla 1=si, 0=no
var coma = 0; // estado coma decimal 0=no, 1=si
var ni = 0; // numero oculto o en espero
var op = "no" // operacion en curso no = sin operacion;

var $num1 = document.getElementById("num1");
var $num2 = document.getElementById("num2");
var $num3 = document.getElementById("num3");
var $num4 = document.getElementById("num4");
var $num5 = document.getElementById("num5");
var $num6 = document.getElementById("num6");
var $num7 = document.getElementById("num7");
var $num8 = document.getElementById("num8");
var $num9 = document.getElementById("num9");
var $num0 = document.getElementById("num0");

var $sumar = document.getElementById("sum");
var $restar = document.getElementById("rest");
var $multi = document.getElementById("multi");
var $dividir = document.getElementById("entre");

var $igual = document.getElementById("igual");
var $retro = document.getElementById("retro");
var $borPar = document.getElementById("borradoParcial");
var $borTotal = document.getElementById("borradoTotal");

var $raiz = document.getElementById("raiz");

var $decimal = document.getElementById("decimal");
var $porcen = document.getElementById("porcen");
var $inver = document.getElementById("inver");

$num1.addEventListener("click", function(){ numero('1'); });
$num2.addEventListener("click", function(){ numero('2'); });
$num3.addEventListener("click", function(){ numero('3'); });
$num4.addEventListener("click", function(){ numero('4'); });
$num5.addEventListener("click", function(){ numero('5'); });
$num6.addEventListener("click", function(){ numero('6'); });
$num7.addEventListener("click", function(){ numero('7'); });
$num8.addEventListener("click", function(){ numero('8'); });
$num9.addEventListener("click", function(){ numero('9'); });
$num0.addEventListener("click", function(){ numero('0'); });
$decimal.addEventListener("click", function(){ numero('.'); });

$sumar.addEventListener("click", function(){ operar('+') });
$restar.addEventListener("click", function(){ operar('-') });
$multi.addEventListener("click", function(){ operar('*') });
$dividir.addEventListener("click", function(){ operar('/') });

$igual.addEventListener("click", function(){ igualar() });

$retro.addEventListener("click", retro);
$borPar.addEventListener("click", borradoParcial);
$borTotal.addEventListener("click", borradoTotal);

$raiz.addEventListener("click", raiz);
$porcen.addEventListener("click", porcentaje);
$inver.addEventListener("click", invertir);

function numero(xx){ // recoge el numero pulsado
    if(x == "0" || xi == 1){ // inicializar 
        pantalla.innerHTML = xx; //mostramos en pantalla
        x = xx; // guardo el numero
        if(xx == ".") {
            pantalla.innerHTML = "0."; // escribimos 0.
            x = xx; // guardo el numero
            coma = 1; // cambiar estado de la coma
        }
    } else { // continuar escribiendo un numero
        if(xx == "." && coma == 0){ // si escribimos una coma decimal por primera vez
            pantalla.innerHTML += xx;
            x += xx;
            coma = 1; // cambiar el estado de la coma
        } // si intentamos escribir una segunda coma decimal no realizar ninguna accion
        else if(xx == "." && coma == 1) {}
        else {
            pantalla.innerHTML += xx;
            x += xx;
        }
    }
    xi = 0; // el numero esta iniciado y podemos ampliarlo
}

function operar(s){
    ni = x; // colocamos el 1er numero en  "numero en espera" para poder escribir el 2do
    op = s; // guardar el tipo de operacion que voy a realizar
    xi = 1; // inicializar pantalla
}

function igualar(){
    if(op == "no"){
        pantalla.innerHTML = x;
    } else {
        sl = ni + op + x; // escribimos la operacion en una cadena
        sol = eval(sl); // convertimos la cadena a codigo y resolvemos
        pantalla.innerHTML = sol; // escribimos el resultado en pantalla
        x = sol; // guardamos la solucion para realizar otra operacion
        op = "no"; // ya no tenemos operaciones pendientes
        xi = 1; // se puede reiniciar la pantalla
    }
}

function retro(){
    cifras = x.length; // hayar numero de caracteres en pantalla
    uc = x.substr(cifras - 1, cifras); // informacion del ultimo caracter
    x = x.substr(0, cifras - 1); // quitamos el ultimo caracter

    if(x == ""){
        x = "0";
    } // si ya no quedan caracteres colocamos el 0

    if(uc == "."){
        coma = 0;
    } // si hemos quitado la coma, se permite escribirla de nuevo

    pantalla.innerHTML = x; // mostramos resultado en pantalla
}

function borradoParcial(){
    pantalla.innerHTML = 0; // borrado de pantalla
    x = 0; // Borrado indicador numero en pantalla
    coma = 0; // reiniciamos el uso de la coma
}

 function borradoTotal(){
    pantalla.innerHTML = 0; // borrado de pantalla
    x = 0; // Borrado indicador numero en pantalla
    coma = 0; // reiniciamos el uso de la coma
    ni = 0; // iniciamos el numero de espera a 0
    op = "no";
}

function raiz(){
    x = Math.sqrt(x);
    pantalla.innerHTML = x;
    op = "no";
    xi = 1;
}

function porcentaje(){
    x = x / 100; // Dividimos el numero entre 100
    pantalla.innerHTML = x;
    igualar();
    xi = 1;
}

function opuesto(){
    nx = Number(x); // Convertimos en numero
    nx = -nx; // Cambiamos el signo
    x = String(nx); // Reconvertimos en string
    pantalla.innerHTML = x; // Mostrar en pantalla
}

function invertir(){
    nx = Number(x); // Convertimos en numero
    nx = (1 / nx); // Cambiamos el signo
    x = String(nx); // Reconvertimos en string
    pantalla.innerHTML = x; // Mostrar en pantalla
    xi = 1; // Reiniciar pantalla al pulsar otro numero
}

function teclado(elEvento){
    evento = elEvento || window.event;
    k = evento.keyCode; // numero de codigo de la tecla.
    // teclas numericas del teclado alfanumerico
    if (k > 47 && k < 58){
        p =k -48; // buscar numero a mostar
        p = String(p); // Convertir a cadena para poder añadir en pantalla
        numero(p); // enciar para mostrar en pantalla
    }
    // Teclas del teclado numero. Seguimos el mismo procedimiento que en el anterior.
    if (k > 95 && k < 106){
        p = k - 96;
        p = String(p);
        numero(p);
    }
    if (k == 110 || k == 190){
        numero(".");
    } // teclas de coma decimal
    if (k == 106){
        operar("*")
    } //tecla multiplicacion
    if (k == 107){
        operar("+");
    } // tecla suma
    if (k == 109){
        operar("-");
    } // tecla resta
    if (k == 111){
        operar("/");
    } // tecla division
    if (k == 32 || k == 13){
        igualar();
    } // tecla igual: intro o barra espaciadora
    if (k == 46){
        borradoTotal();
    } // tecla borrado total: "supr"
    if (k == 8){
        retro();
    } // Retroceso en escritura tecla retroceso.
    if (k == 36){
        borradoParcial();
    } // tecla borrado parcial; tecla de inicio
}