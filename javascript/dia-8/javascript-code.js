// Comentarios

// Comentario Corto

/*
Comentario largo o Múltiples Líneas
 */

 //Gramática de Javascript

 a = "Hola Mundo"
 alert(a)

 // ó

 a = "Hola Mundo";
 alert(a);

 //  Bloques de Código
 {
     // Este bloque contiene dos sentencias
     var a = "Hola";
     alert(a);
 }

 // Tipos de Datos
/*
string
number
Boolean
undefined
null
*/
let nombre = 'Andres'; typeof nombre; // string
let edad = 5; typeof edad; // number
let soltero = true; typeof soltero; // boolean
typeof apellido; // undefined
let flag = null; typeof flag; // object
let estudiante = {nombre: "Andres", apellido = "Imery"}; typeof estudiante; // object


// Escape de Caracteres
let apellido = "D\'ambrosio"; // escapando comillas simples
let mensaje = " cita: \"Esto es una cita\" "; // escapando comillas dobles
let mensaje2 = "Esto es mi primera línea \n y esto va a la segunda línea"; // \n nueva linea
let mensaje3 = "Eso es otra prueba \r"; // \r {Enter} Retorno de carga
let mensaje4 = "Esto esta separado por \t tabulación" // \t Tabulado

// Reglas para definir una variable

$nombre = "andres"; // Puedo comenzar con $
_nombre = "andres"; // Puedo comenzar con _
nombre = "andres"; // Puedo definirla solo letras
primer_nombre = "Andres"; // Puedo separarla con _
primerNombre = "Andres"; // Puedo definirla usando camelCase
nombre1 = "Andres"; // Puedo incluir números pero no comenzar on número

// Todas las variables son "case sensitive", NOMBRE no es igual a Nombre o NomBre

// Palabras reservadas

/*

abstract, boolean, break, byte, case, catch, char, class, const,
continue, debugger, default, delete, do, double, else, enum,
export, extends, false, final, finally, float, for, function,
goto, if, implements, import, in instanceof, int, interface,
long, native, new, null, package, private, protected, public,
return, short, static, super, switch, synchronized, this, throw,
throws, transient, true, try, typeof, var, volatile, void, while,
with

 */

 // Asignaciones

 var a = "Hola";

 let a, b, c;

 let a = 1, b, c = 5;

 b = 7;