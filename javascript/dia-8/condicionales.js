// Sentencia IF, IF ELSE, IF ELSE IF ELSE

var a = 5;
var b = 3;
var c = 8;

if(a > b || b != 0){
    // sentencias a ejecutar
}

if(a > b){
    console.log("a es mayor que b");
}else{
    console.log("b es mayor que a");
}

if(a > b && a > c){
    console.log("A es el mayor");
}else if(b > a && b > c){
    console.log("B es el mayor");
}else{
    console.log("C es el mayor");
}

var edad = 13;
if(edad < 18){
    console.log("Lo siento, no puede ver este contenido");
}

// Operador Ternario

var n = 5;
// Condicion IF Verdadero ELSE Falso
n%2===0 ? console.log("Es par") : console.log("Es impar");

var flag = true;
flag ? alert("Verdadero") : alert("Falso");

edad = 21;
edad >= 18 ? console.log("Es mayor") : console.log("Es menor");

// Sentencia Switch
var opt = 4;

if(opt === 1){
    console.log("Opción del Menú 1");
}else if(opt === 2){
    console.log("Opción del Menú 2");
}else if(opt === 3){
    console.log("Opción del Menú 3");
}else if(opt === 4){
    console.log("Opción del Menú 4");
}else if(opt === 5){
    console.log("Opción del Menú 5");
}else{
    console.log("Opción del Menú Incorrecta");
}

switch(opt){
    case 1:
        console.log("Opción del Menú 1");
        break;
    case 2:
        console.log("Opción del Menú 2");
        break;
    case 3:
        console.log("Opción del Menú 3");
        break;
    case 4:
        console.log("Opción del Menú 4");
        break;
    case 5:
        console.log("Opción del Menú 5");
        break;
    default:
        console.log("Opción del Menú Incorrecta");
        break;
}

