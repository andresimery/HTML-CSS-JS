function saludo(){
    console.log("Invocando una función (saludo)");
}

saludo();

var saludo2 = function(){
    console.log("Invocando una función (saludo2)");
}

saludo2();

function mayor(){
    var edad = 18
    if(edad < 18){
        return "Es menor";
    }else{
        return "Es mayor";
    }
}

console.log(mayor());

function mayor2(){
    var edad = 18
    if(edad < 18){
        return false;
    }else{
        return true;
    }
}

if(mayor2()){
    console.log("Es mayor");
}else{
    console.log("Es menor");
}

function sumar(a, b){
    return a + b;
}

console.log(sumar(40, 23));


var counter = 0;
function contar(){
    var suma = 0;
    console.log(suma);
    return counter+=1;
}
//console.log(suma);
console.log(contar());

// uso del forEach

var arr = [12, 34, 45, 54];
arr.forEach(function(nota, index/*valor, posicion*/){
    console.log("Nota: " + index/*posicion*/ + " - " + nota/*valor*/);
});

function square(valor){
    return valor * valor;
}

arr.map(function(valor, index){
    console.log(valor * valor);
});

total = arr.reduce(function(anterior, siguiente){
    return anterior + siguiente;
});
console.log(total);

numeros = arr.filter(function(numero){
    return numero%2===0;
});
console.log(numeros);

var nombres = ["Luis", "Pedro", "Miguel", "Juan"];
nombre = nombres.filter(function(nombre){
        if(nombre.toLocaleLowerCase().indexOf("j") != -1 ){
            return nombre;
        }
});
console.log(nombre);