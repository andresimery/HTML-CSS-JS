// Ciclos While, do while, for, for anidados.

var n = 1;
while(n <= 10){
    console.log(n);
    n++;
} // Se va a mantner ejecutando mientras la condición sea verdadera

n = 1;
do {
    console.log(n)
    n++;
}while(n <= 10); 
// Se va a mantner ejecutando mientras la condición sea verdadera,
// y se ejecutara solo una vez

// Ciclo For(inicializador; condición; incremental)
for(var i = 1; i <= 10; i++){
    console.log(i);
}

var multi = [[1, 1], [10, 10], [20, 20]]

for(var i = 0; i < multi.length; i++){
    for(var j = 0; j < multi[i].length; j++){
        console.log(multi[i][j]);
    }
}