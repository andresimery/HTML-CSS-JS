// Objetos en JavaScript
var superman = {
    nombre: "Superman",
    "nombre real": "Clark Kent",
    volar: function(){},
    laser: function(){},
}
//var superman = new Object()

console.log(superman["nombre real"]);
console.log(superman.nombre);
console.log(superman.volar());
console.log(superman["volar"]());

// Saber si existe una propiedad en un Objeto
console.log("edad" in superman);
console.log(superman.edad !== undefined);

// Saber todas las propiedades del Objeto
for(var key in superman){
    console.log(key + ":" + superman[key]);
}

// Añadir propiedades al Objeto
superman.ciudad = "Metropolis";
console.log(superman);

// Eliminar una propiedad del Objeto
delete superman.ciudad;

// Objeto Anidado o Conjunto de Objetos
justice_league = {
    superman: { realName: "Clark Kent"},
    batman: { realName: "Bruce Wayne"},
    wonderWoman: { realName: "Diana Prince"}
}

console.log(justice_league.superman.realName);

function saludo(options){
    options = options || {};
    var saludo = options.saludo || "Hola";
    var nombre = options.nombre || "visitante";
    var edad = options.edad || 18;
    return saludo + "! Mi nombre es: " + nombre + " y tengo " + edad + " de edad";
}
datos = {nombre: "Andres Eduardo", edad: 18 }
console.log(saludo(datos));
console.log(saludo({nombre: "Carlos", edad: 20 }));

// JavaScript Object Notation or JSON
var alumno = '{"nombre": "Pedro", "edad": 38, "ciudad": "Caracas"}'
alumno = JSON.parse(alumno);
console.log(alumno.nombre);
console.log(alumno.edad);

var clark = JSON.stringify(superman);
console.log(clark);

// Objeto Math
console.log(Math.PI);
console.log(Math.LN2)

var sueldo = 1320.51;
console.log(Math.ceil(sueldo));
console.log(Math.floor(sueldo));
console.log(Math.round(sueldo));

console.log(Math.random());
console.log(Math.floor(Math.random() * 5) + 1);

console.log(Math.abs(1345.456));

console.log(Math.pow(3, 2));

// Objeto Date();
hoy = new Date('2018 06 23');
console.log(hoy);

// Devuelve el dia actual
console.log(hoy.getDay());


var dias = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"];
var meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
var fecha =  new Date();

console.log(dias[fecha.getDay()]);
console.log(meses[fecha.getMonth()]);
console.log(fecha.getHours()+":"+fecha.getMinutes());

// Expresiones Regulares

var pattern_1 = /\w+ing/;
var pattern_2 = new RegExp('\w+ing');

prueba = pattern_1.test("Clean");
console.log(prueba);

console.log(pattern_1.exec("Joke"));
console.log(pattern_1.exec("Joking"));

var soloVocales = /[aeiou]/;
console.log(soloVocales.test("test"));

var alfabeto = /[A-Z]/;
var numeros = /[0-9]/;
var alfabeto = /[^A-Z]/;

// Caracteres Especiales
// \w = [A-Za-z0-9 ] // Coincide con cualquier caracter que sea una palabra
// \W = [^A-Za-z0-9 ] // Coincide con cualquier caracter que no sea una palabra
// \d = [0-9] // Coincide con cualquier caracter que sea un digito
// \D = [^0-9] // Coincide con cualquier caracter que no sea un digito
// \s = [\t\r\n\f] // Coincide con cualquier caracter que sea de espacio en blanco
// \S = [^\t\r\n\f] // Coincide con cualquier caracter que no sea de espacio en blanco

// Propiedades de las Expresiones Regulares
/[a-z]/g 
/[a-z]/i
/[a-z]/m