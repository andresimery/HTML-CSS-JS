var quiz = [
    ["Cúal es el nombre real de Superman?", "Clark Kent"],
    ["Cúal es el nombre real de Spiderman?", "Peter Parker"],
    ["Cúal es el nombre real de Batman?", "Bruce Wayne"],
    ["Cúal es el nombre real de Wonderwoman?", "Diana Prince"],
];

var score = 0;

for(var i = 0, max=quiz.length; i < max; i++ ){
    var respuesta = prompt(quiz[i][0]);
    if(respuesta === quiz[i][1]){
        alert("Respuesta Correcta");
        score += 10;
    }else{
        alert("Respuesta Incorrecta");
        score -= 5;
    }
}

alert("Game Over, Su puntuación fue " + score + " puntos");